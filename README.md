# Analysis of energy trends in the period 1980-2017 #

The aim of this project is to investigate historical trends in energy production and consumption via data analysis in Python.

I created this project out of interest in the energy sector. There are two sets of questions I am trying to answer with this analysis:
 - _Quantitative_ questions: who is the top producer/consumer per energy source? Who has increased its production/consumption the most? Etc.
 - _Qualitative_ questions: why do we see a specific variation? Are trends due to natural market evolutions, or to deliberate country policies? Etc.

The current version contains:
 1. Cleanup of the dataset
 2. Quick overview of some selected arbitrary countries
 3. Analysis of the top producers and consumers per energy source, with trends
 4. Analysis of the relative variations over the years of selected productions
 5. Analysis of the top net producers and consumers per energy source, with trends

## Code ##

The file [energy.ipynb](energy.ipynb) contains the actual analysis, with all the code, the charts and the comments and deductions one can make (or try to make). It is a jupyter notebook file that you can visualise directly in the browser.

For ease of use and transparency, the CSV file [INT-Export-02-05-2020_10-33-34.csv](INT-Export-02-05-2020_10-33-34.csv) with the data is included in the repository.

## Technologies ##

Python 3.5.2, Numpy 1.18.2, Pandas 0.24.2, Matplotlib 3.0.2, Seaborn 0.9.0

## Data source and licensing ##

The currently used data is updated at February 5, 2020.

The data comes from the U.S. Energy Information Administration (EIA) and is available online at:

https://www.eia.gov/international/data/world

This data is also available through the U.S. Government Data portal, at the following URL:

https://catalog.data.gov/dataset/international-energy-statistics

As specified in the last link, at the time of writing the data is licensed as Public Domain.
